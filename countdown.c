///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time: Sat Mar  3 17:25:00 2001
//   Time difference: 19 years 340 days 0 hours 52 minutes 58 seconds
//   Time difference: 19 years 340 days 0 hours 52 minutes 59 seconds
//   Time difference: 19 years 340 days 0 hours 53 minutes 0 seconds

//
// @author Eliya Nakamura <eliyanak@hawaii.edu>
// @date   4 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
   
   // struct to hold reference date
   struct tm ref_struct;
   // struct pointer to hold current date
   struct tm *current_struct;
   // variable to hold data for reference time
   time_t reference;
   // variable to hold data for current time
   time_t current;
   // variables to hold difference between times
   double difference;
   time_t diff_t;
   // variables for time difference breakdown
   int ref_year, current_year, years, days, hours, minutes, seconds;

   // set reference data and store in reference struct
   ref_year = 2001;
   ref_struct.tm_year = ref_year - 1900;
   ref_struct.tm_mon = 2;
   ref_struct.tm_mday = 3;
   ref_struct.tm_hour = 17;
   ref_struct.tm_min = 25;
   ref_struct.tm_sec = 0;
   ref_struct.tm_isdst = 0;

   // convert reference time struct to time_t
   reference = mktime( &ref_struct );

   printf("Reference time and date: %s", asctime(&ref_struct));

   while (1) {
   // get current time values and store it in current
   time( &current );
   // fill current struct with the values for current time
   current_struct = localtime( &current );
  
   // find difference 
   difference = difftime(current, reference);

   // adjust if reference date is after current date
   if (difference < 0) {
      difference = difftime(reference, current);
      current_year = ref_year - (difference / 31557600);
      //printf("DEBUG:current year: %d\n", current_year);
      // account for the time lost due to leap year (does not work but this was my attempt at it)
      /*if (current_year % 400 != 0){
         if (current_year % 4 == 1) {
            difference -= 3600 * 6;
         } else if (current_year % 4 == 2) {
            difference -= 3600 * 2 * 6;
         } else if (current_year % 4 == 3) {
            difference -= 3600 * 3 * 6;
         }
      }*/
   } else {
      // account for the time lost due to leap year
      if (ref_year % 400 != 0){
         if (ref_year % 4 == 1) {
            difference -= 3600 * 6;
         } else if (ref_year % 4 == 2) {
            difference -= 3600 * 2 * 6;
         } else if (ref_year % 4 == 3) {
            difference -= 3600 * 3 * 6;
         } 
      }
   }
   // convert seconds to years, days, hours, minutes, seconds
   years = difference / 31557600; 
   days = (difference - (years * 31557600)) / 86400;
   hours = (difference - (years * 31557600) - (days * 86400)) / 3600;
   minutes = (difference - (years * 31557600) - (days * 86400) - (hours * 3600))/ 60;
   seconds = difference - (years * 31557600) - (days * 86400) - (hours * 3600) - (minutes * 60);

   //printf("DEBUG:Current local time and date: %s", asctime(current_struct));
   
   printf("Time difference: %d years %d days %d hours %d minutes %d seconds\n", years, days, hours, minutes, seconds);

   sleep(1);
   }

   return 0;
}
